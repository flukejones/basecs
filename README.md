
## Model

```
World:
    |-> Components (generic Any)
    |       |-> Component<T>
    |       |       |-> T
    |       |       |-> Pointer -> Owning Entity
    |       |-> Component<T>
    |       |       |-> T
    |       |       |-> Pointer -> Owning Entity
    |-> Entites:
    |       |-> Pointer -> Owned Component<T>
    |       |-> Pointer -> Owned Component<T>
    |-> Types:
        |-> (type_id: index in-to Components)
``` 

Pointers break the chain of ownership. The pointers are required because Rust can not
allow mutable borrows within such a structure - how do you enforce that an entity borrowed
from a component can not then borrow that same component back? Or if a component borrowed
from the `Components` collection won't conflict with the same self borrowed from an
entity reference? Which ownership is the valid one?

It would be very easy to fall in to a circular reference here.

And so because the entities on their own are nothing more than *collections* of 
components, the ownership tracking must fall to the components, as these are what
we will work with regardless of where it has come from. 

The reason the entity has a pointer to all components is so you can iterate over the
component list and check each relevant one. For the components, they have a pointer
to the owning entity so that it is easy to check if the entity has another component
which may be relevant at that time.

### Destroying an entity

A component may not end it's own life, it must be done by an Entity or the World that it
is contained in. For the entity to be able to remove an appendage it must also have
a reference to the container that the component lives in, so that the component may be
removed from the collection correctly. 
