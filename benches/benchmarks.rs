#![allow(dead_code)]
use basecs::World;
use bencher::{benchmark_group, benchmark_main, black_box, Bencher};

/// Entities with velocity and position component.
/// Used to insert in staggered order
pub const N_POS_VEL_MODULUS: usize = 10;

/// Entities with position and velocity component
pub const N_POS_VEL: usize = 1_000;

/// Entities with position component only
pub const N_POS: usize = 10_000;

#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct Velocity {
    pub dx: f32,
    pub dy: f32,
}

fn build() -> World {
    let mut world = World::default();
    assert!(world.new_collection::<Position>(N_POS).is_ok());
    assert!(world.new_collection::<Velocity>(N_POS_VEL).is_ok());

    for _ in 0..N_POS - N_POS_VEL {
        let mut x = world.new_entity();
        world.add_to(&mut x, Position { x: 1.1, y: 2.2 }).unwrap();
    }
    for _ in 0..N_POS_VEL {
        let mut x = world.new_entity();
        world.add_to(&mut x, Position { x: 1.1, y: 2.2 }).unwrap();
        world
            .add_to(
                &mut x,
                Velocity {
                    dx: 5.0,
                    dy: 1.4325,
                },
            )
            .unwrap();
    }
    world
}

fn bench_update_deref(b: &mut Bencher) {
    let mut world = build();

    let vel = world.borrow_mut::<Velocity>().unwrap();

    b.iter(|| {
        black_box(for vel in vel.iter_mut() {
            let dx = vel.dx;
            let dy = vel.dy;
            let mut pos = vel.entity_as_mut().get_mut::<Position>();
            //let vel = vel.try_borrow().unwrap();
            pos.x += dx;
            pos.y += dy;
            //accum += vel.dy;
        })
    });
}

fn bench_update_try(b: &mut Bencher) {
    let mut world = build();

    let vel = world.borrow_mut::<Velocity>().unwrap();

    b.iter(|| {
        black_box(for item in vel.iter_mut() {
            if let Ok(vel) = item.try_borrow() {
                if let Ok(mut pos) = item.entity_as_mut().try_get_mut::<Position>() {
                    pos.x += vel.dx;
                    pos.y += vel.dy;
                }
            }
        })
    });
}

fn bench_build(b: &mut Bencher) {
    b.iter(|| build());
}

benchmark_group!(benches, bench_build, bench_update_deref, bench_update_try,);
benchmark_main!(benches);
