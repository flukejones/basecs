use crate::component::{CompRef, CompRefMut, Component};
//use std::any::type_name;
use crate::errors::ECSError;
use std::any::{type_name, Any, TypeId};
use std::ptr::null_mut;

#[derive(Debug, Default, Clone)]
pub struct Entity {
    // ID is the entities slot index in the world
    pub(crate) id: usize,
    //stores ComponentCell<T>
    pub(crate) links: Vec<(TypeId, *mut dyn Any)>,
}

impl Entity {
    #[inline]
    fn find_link_ptr<'a, T: 'static>(&self) -> *mut Component<T> {
        let type_id = TypeId::of::<Component<T>>();

        for (stored_id, link) in &self.links {
            if stored_id == &type_id {
                return link.cast::<Component<T>>();
            }
        }
        null_mut()
    }

    // TODO: convert these to results
    #[inline]
    pub fn try_get<'a, T: 'static>(&self) -> Result<CompRef<T>, ECSError> {
        let ptr = self.find_link_ptr::<T>();
        if !ptr.is_null() {
            unsafe { return (*ptr).try_borrow() }
        }
        Err(ECSError::NoComponentFor(type_name::<T>().to_owned()))
    }

    #[inline]
    pub fn try_get_mut<'a, T: 'static>(&self) -> Result<CompRefMut<T>, ECSError> {
        let ptr = self.find_link_ptr::<T>();
        if !ptr.is_null() {
            unsafe { return (*ptr).try_borrow_mut() }
        }
        Err(ECSError::NoComponentFor(type_name::<T>().to_owned()))
    }

    /// Unchecked deref to inner `T`. Will panic if the requested type
    /// does not exist.
    #[inline]
    pub fn get<T: 'static>(&self) -> &T {
        let ptr = self.find_link_ptr::<T>();
        if !ptr.is_null() {
            unsafe {
                return &(*ptr);
            }
        }
        panic!("Component {} does not exist", type_name::<T>());
    }

    /// Unchecked deref_mut to inner `T`. Will panic if the requested type
    /// does not exist.
    #[inline]
    pub fn get_mut<T: 'static>(&mut self) -> &mut T {
        let ptr = self.find_link_ptr::<T>();
        if !ptr.is_null() {
            unsafe {
                return &mut (*ptr);
            }
        }
        panic!("Component {} does not exist", type_name::<T>());
    }

    #[inline]
    pub(crate) fn add_link<T: 'static>(&mut self, c: &mut Component<T>) {
        let type_id = TypeId::of::<Component<T>>();
        self.links.push((type_id, c as *mut Component<T>));
    }
}
