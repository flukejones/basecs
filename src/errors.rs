use std::error::Error;
use std::fmt;
use std::fmt::{Debug, Display};

#[derive(Debug)]
pub enum ECSError {
    Downcast,
    DowncastMut,
    NoComponentMap,
    BitMasksExhausted,
    NoCollectionFor(String),
    NoComponentFor(String),
    Mutable(String),
    Immutable(String),
}

impl Display for ECSError {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ECSError::Downcast => Display::fmt("could not downcast to ref", f),
            ECSError::DowncastMut => Display::fmt("could not downcast to mut ref", f),
            ECSError::NoComponentMap => Display::fmt("no component map for type", f),
            ECSError::BitMasksExhausted => {
                Display::fmt("bitmasks exhausted, can't add new component type", f)
            }
            ECSError::NoCollectionFor(s) => Display::fmt(&format!("no collection for {}", s), f),
            ECSError::NoComponentFor(s) => {
                Display::fmt(&format!("entity does not have a {}", s), f)
            }
            ECSError::Immutable(s) => Display::fmt(
                &format!(
                    "can not borrow, component {} is mutably borrowed elsewhere",
                    s
                ),
                f,
            ),
            ECSError::Mutable(s) => Display::fmt(
                &format!(
                    "can not borrow mutably, component {} is immutable borrowed elsewhere",
                    s
                ),
                f,
            ),
        }
    }
}

impl Error for ECSError {}
