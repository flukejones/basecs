use crate::component::Component;
use crate::entity::Entity;
use crate::errors::ECSError;
use hashbrown::HashMap;
use persist_o_vec::Persist;
use std::any::{type_name, Any, TypeId};
use std::cell::RefCell;

pub mod component;
pub mod entity;
pub mod errors;

#[cfg(feature = "component_max_31")]
type BitMaskType = u32;
#[cfg(feature = "component_max_63")]
type BitMaskType = u64;
#[cfg(feature = "component_max_127")]
type BitMaskType = u128;

#[cfg(feature = "component_max_31")]
const BIT_MASK_MAX: BitMaskType = 31;
#[cfg(feature = "component_max_63")]
const BIT_MASK_MAX: BitMaskType = 63;
#[cfg(feature = "component_max_127")]
const BIT_MASK_MAX: BitMaskType = 127;

/// This is the root of the ECS implementation
#[derive(Debug, Default)]
pub struct World {
    // the index in to this Vec is the ID of the entity
    entities: Vec<Entity>,
    // in the box is Vec<RefCell<Persist<Component<T>>
    components: Vec<RefCell<Box<dyn Any>>>,
    // link direct to collection for fast lookup
    type_links: HashMap<TypeId, usize>,
}

impl World {
    #[inline]
    pub fn new_collection<T: 'static>(&mut self, capacity: usize) -> Result<(), ECSError> {
        let type_id = TypeId::of::<T>();
        // if the component doesn't already have a type_mask entry
        if !self.type_links.contains_key(&type_id) {
            if (self.type_links.len() as BitMaskType) >= BIT_MASK_MAX {
                return Err(ECSError::BitMasksExhausted);
            }
            // insert new vec for type
            let inner = Persist::<Component<T>>::with_capacity(capacity);
            let container = RefCell::new(Box::new(inner));

            //
            self.components.push(container);
            self.type_links.insert(type_id, self.components.len() - 1);
            return Ok(());
        }
        Ok(())
    }

    #[inline]
    pub fn new_entity(&mut self) -> Entity {
        // TODO: reuse slots
        self.entities.push(Entity {
            id: self.entities.len(),
            links: Vec::new(),
        });
        return unsafe { self.entities.get_unchecked(self.entities.len() - 1).clone() };
    }

    #[inline]
    pub fn add_to<T: 'static>(&mut self, entity: &mut Entity, part: T) -> Result<(), ECSError> {
        // get type masks and index to components
        let type_id = TypeId::of::<T>();
        if let Some(type_link) = self.type_links.get_mut(&type_id) {
            // anchor the cast
            let mut collection =
                unsafe { self.components.get_unchecked_mut(*type_link).borrow_mut() };
            let collection = collection
                .downcast_mut::<Persist<Component<T>>>()
                .ok_or(ECSError::DowncastMut)?;

            let stored_entity = unsafe { self.entities.get_unchecked_mut(entity.id) };
            if let Some(comp_index) = collection.push(Component::new(part, stored_entity)) {
                if let Some(comp_ptr) = collection.get_mut(comp_index) {
                    // need to update in two places at the moment
                    stored_entity.add_link(comp_ptr);
                    entity.add_link(comp_ptr);
                }
            }
            return Ok(());
        }

        // todo: check for vacated slots in world and components
        Err(ECSError::NoCollectionFor(format!(
            "Collection for {} does not exist",
            type_name::<T>()
        )))
    }

    #[inline]
    pub fn rm_entity(&mut self, ent: Entity) {}

    #[inline]
    pub fn borrow_mut<T: 'static>(&mut self) -> Result<&mut Persist<Component<T>>, ECSError> {
        let type_id = TypeId::of::<T>();
        if let Some(type_index) = self.type_links.get(&type_id) {
            //
            let collection = unsafe { self.components.get_unchecked_mut(*type_index).get_mut() };
            let collection = collection
                .downcast_mut::<Persist<Component<T>>>()
                .ok_or(ECSError::DowncastMut)?;
            return Ok(collection);
        }
        Err(ECSError::NoComponentMap)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct Test1 {
        x: u32,
    }

    struct Test2(u32);

    struct Test3 {
        xz: u32,
    }

    #[test]
    fn entity() {
        let mut world = World::default();
        assert!(world.new_collection::<Test1>(100).is_ok());
        assert!(world.new_collection::<Test2>(100).is_ok());
        assert!(world.new_collection::<Test3>(100).is_ok());

        let mut entity1 = world.new_entity();
        world.add_to(&mut entity1, Test1 { x: 42 }).unwrap();
        {
            assert_eq!(entity1.try_get::<Test1>().unwrap().x, 42);
        }
        let mut y = entity1.try_get_mut::<Test1>().unwrap();
        y.x += 1;
        assert_eq!(y.x, 43);

        let mut entity2 = world.new_entity();
        world.add_to(&mut entity2, Test2(42)).unwrap();

        let mut entity3 = world.new_entity();
        world.add_to(&mut entity3, Test3 { xz: 42 }).unwrap();
        entity3.try_get_mut::<Test3>().unwrap().xz += 5;
        assert_eq!(entity3.try_get::<Test3>().unwrap().xz, 47);
    }

    #[test]
    fn debug_bench() {
        pub const N_POS_VEL: usize = 1_000;
        pub const N_POS: usize = 10_000;

        #[derive(Copy, Clone, Debug, PartialEq, Default)]
        pub struct Position {
            pub x: f32,
            pub y: f32,
        }

        #[derive(Copy, Clone, Debug, PartialEq, Default)]
        pub struct Velocity {
            pub dx: f32,
            pub dy: f32,
        }

        let mut world = World::default();
        assert!(world.new_collection::<Position>(N_POS).is_ok());
        assert!(world.new_collection::<Velocity>(N_POS_VEL).is_ok());

        for _ in 0..N_POS - N_POS_VEL {
            let mut x = world.new_entity();
            world.add_to(&mut x, Position { x: 1.1, y: 2.2 }).unwrap();
        }
        for _ in 0..N_POS_VEL {
            let mut x = world.new_entity();
            world.add_to(&mut x, Position { x: 1.1, y: 2.2 }).unwrap();
            world
                .add_to(
                    &mut x,
                    Velocity {
                        dx: 5.0,
                        dy: 1.4325,
                    },
                )
                .unwrap();
        }

        let vel = world.borrow_mut::<Velocity>().unwrap();

        for _ in 0..1000 {
            for vel in vel.iter_mut() {
                let dx = vel.dx;
                let dy = vel.dy;
                let mut pos = vel.entity_as_mut().get_mut::<Position>();
                //let vel = vel.try_borrow().unwrap();
                pos.x += dx;
                pos.y += dy;
                //accum += vel.dy;
            }
        }
    }
}
