use crate::entity::Entity;
use crate::errors::ECSError;
use core::sync::atomic::{AtomicUsize, Ordering};
use std::any::type_name;
use std::fmt::Debug;
use std::ops::{Deref, DerefMut};

const MUTBORROW: usize = 1usize.rotate_right(1);

#[derive(Debug)]
pub struct CompRef<T: 'static + Sized>(*mut Component<T>);

impl<T: 'static> Deref for CompRef<T> {
    type Target = T;

    /// Will panic at runtime if there are any current mutable borrows
    #[inline]
    fn deref(&self) -> &T {
        unsafe {
            return &(*self.0).v;
        }
    }
}

impl<T: 'static> Drop for CompRef<T> {
    #[inline]
    fn drop(&mut self) {
        unsafe { (*self.0).release() }
    }
}

#[derive(Debug)]
pub struct CompRefMut<T: 'static>(*mut Component<T>);

impl<T: 'static> Deref for CompRefMut<T> {
    type Target = T;

    /// Will panic at runtime if there are any current mutable borrows
    #[inline]
    fn deref(&self) -> &T {
        unsafe {
            return &(*self.0).v;
        }
    }
}

impl<T: 'static> DerefMut for CompRefMut<T> {
    /// Will panic at runtime if there are any current mutable borrows
    #[inline]
    fn deref_mut(&mut self) -> &mut T {
        unsafe {
            return &mut (*self.0).v;
        }
    }
}

impl<T: 'static> Drop for CompRefMut<T> {
    #[inline]
    fn drop(&mut self) {
        unsafe { (*self.0).release() }
    }
}

/// A Component will deref to the inner `T`. This follows all normal borrow rules.
///
/// The alternative thread-safe borrow methods are `try_borrow` and `try_borrow_mut`.
/// If for example you are doing single threaded iteration over the containing
/// storage (`Persist`) then you should be safe to use standard deref as long as
/// you follow the normal borrow rules. Otherwise you should use the `try_` methods.
///
/// Do keep in mind that combined use of `try_` and deref in separate threads in UB.
#[derive(Debug)]
pub struct Component<T: Sized> {
    v: T,
    // must not be public
    entity_link: *mut Entity,
    borrow_marker: AtomicUsize,
}

impl<T: 'static> Component<T> {
    #[inline]
    pub(crate) fn new(value: T, entity: &mut Entity) -> Self {
        Component {
            v: value,
            entity_link: entity as *mut Entity,
            borrow_marker: AtomicUsize::new(0),
        }
    }

    // a component should never exist without an entity
    #[inline]
    pub fn entity_as_ref(&self) -> &Entity {
        unsafe { &*self.entity_link }
    }

    #[inline]
    pub fn entity_as_mut(&mut self) -> &mut Entity {
        unsafe { &mut *self.entity_link }
    }

    /// Borrow the inner component `T`, returns a thread-safe `CompRef`
    #[inline]
    pub fn try_borrow(&self) -> Result<CompRef<T>, ECSError> {
        // fetch_add is twice as expensive as a load/store
        // this totals 9ns
        let val = self.borrow_marker.load(Ordering::Relaxed);
        if val & MUTBORROW != 0 {
            return Err(ECSError::Immutable(type_name::<T>().to_owned()));
        }
        // Adds to the current value, returning the previous value, add 1 to that
        if val.wrapping_add(1) == 0 {
            // Wrapped, this borrow is invalid!
            core::panic!()
        }
        self.borrow_marker.store(val + 1, Ordering::Relaxed);
        Ok(CompRef(self as *const Self as *mut Self))
    }

    /// Mutably borrow the inner component `T`, returns a thread-safe `CompRefMut`
    #[inline]
    pub fn try_borrow_mut(&mut self) -> Result<CompRefMut<T>, ECSError> {
        // approx 7ns
        if self.borrow_marker.load(Ordering::Relaxed) == 0 {
            self.borrow_marker.store(MUTBORROW, Ordering::Relaxed);
            return Ok(CompRefMut(self as *mut Component<T>));
        }
        Err(ECSError::Mutable(type_name::<T>().to_owned()))
    }

    #[inline]
    pub fn release(&mut self) {
        // detect if immutable or mutable borrowed
        let borrow = self.borrow_marker.load(Ordering::Relaxed);

        if borrow & MUTBORROW == 0 && borrow != 0 {
            // immutable
            self.borrow_marker.fetch_sub(1, Ordering::Relaxed);
        } else if borrow & MUTBORROW == MUTBORROW {
            // mutable
            self.borrow_marker.store(0, Ordering::Relaxed);
        }
        // or do nothing if not borrowed
    }
}

// It is entirely possible to iterate over the containing array and grab a
// ref to a Component while there is an existing borrow out, because the
// borrow chain is broken with `Any`
impl<T: 'static> Deref for Component<T> {
    type Target = T;

    /// Will panic at runtime if there are any current mutable borrows
    #[inline]
    fn deref(&self) -> &T {
        return &self.v;
    }
}

impl<T: 'static> DerefMut for Component<T> {
    /// Will panic at runtime if there are any current immutable borrows
    #[inline]
    fn deref_mut(&mut self) -> &mut T {
        return &mut self.v;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct Test1 {
        x: u32,
    }

    struct Test2(u32);

    struct Test3(u32);

    #[test]
    fn component_borrow() {
        let mut ent = Entity::default();
        let mut component = Component::new(Test1 { x: 1 }, &mut ent);
        let _x = component.try_borrow_mut().unwrap();
        assert!(component.try_borrow().is_err());
    }

    #[test]
    fn component_borrow_mut() {
        let mut ent = Entity::default();
        let mut component = Component::new(Test1 { x: 1 }, &mut ent);
        let _x = component.try_borrow().unwrap();
        assert!(component.try_borrow_mut().is_err());
    }

    //    #[test]
    //    fn component_deref_mut() {
    //        let mut ent = Entity::default();
    //        let mut component = Component::new(Test1 { x: 1 }, &mut ent);
    //        let y = &mut *component;
    //        y.x += 1;
    //        assert_eq!(y.x, 2);
    //    }
    //
    //    #[test]
    //    fn component_deref() {
    //        let mut ent = Entity::default();
    //        let component = Component::new(Test1 { x: 1 }, &mut ent);
    //        let x = &*component;
    //        assert_eq!(x.x, 1);
    //    }

    #[test]
    fn borrow_scopes() {
        let mut ent = Entity::default();
        let mut component = Component::new(Test1 { x: 1 }, &mut ent);
        {
            let mut x = component.try_borrow_mut().unwrap();
            x.x += 2;
        }
        let y = component.try_borrow().unwrap();
        assert_eq!(y.x, 3);
    }

    #[test]
    fn borrow_scopes_bad() {
        let mut ent = Entity::default();
        let mut component = Component::new(Test1 { x: 1 }, &mut ent);
        let _x = component.try_borrow_mut().unwrap();
        assert!(component.try_borrow().is_err());
    }

    #[test]
    fn deref_scopes() {
        let mut ent = Entity::default();
        let mut component = Component::new(Test1 { x: 1 }, &mut ent);
        {
            let mut x = &mut *component;
            x.x += 2;
        }
        let y = &*component;
        assert_eq!(y.x, 3);
    }

    #[test]
    fn multi_immutable_borrows() {
        let mut ent = Entity::default();
        let mut component1 = Component::new(Test1 { x: 1 }, &mut ent);
        ent.add_link(&mut component1);

        // comp-inner is borrowed
        let _x = component1.try_borrow().unwrap();
        let link1 = component1.entity_as_ref().try_get::<Test1>();
        // so shouldn't be able to get a ref through the component-entity link
        assert!(link1.is_ok());
        let link2 = component1.try_borrow();
        assert!(link2.is_ok());
        let link3 = component1.entity_as_ref().try_get::<Test1>();
        // so shouldn't be able to get a ref through the component-entity link
        assert!(link3.is_ok());
    }

    #[test]
    fn component_entity_link_test() {
        let mut ent = Entity::default();

        let mut component1 = Component::new(Test1 { x: 1 }, &mut ent);
        ent.add_link(&mut component1);

        let mut component2 = Component::new(Test2(10), &mut ent);
        ent.add_link(&mut component2);

        let mut component3 = Component::new(Test3(11), &mut ent);
        ent.add_link(&mut component3);

        // Shouldn't be able to borrow Test1 through entity again
        {
            // comp-inner is borrowed mut
            let _x = component1.try_borrow_mut().unwrap();
            let link = component1.entity_as_ref().try_get::<Test1>();
            // so shouldn't be able to get a ref through the component-entity link
            assert!(link.is_err());
        }
        //        {
        //            let x = component1.entity_as_mut().get_mut::<Test1>();
        //            let _y = component1.entity_as_ref().get::<Test1>();
        //            x.x += 1;
        //        }
    }
}
